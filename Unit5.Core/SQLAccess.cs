﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public class SQLAccess
    {
        private SQLiteConnection conn;
        private static object lockData = new object();
        public SQLAccess(string path)
        {
            this.conn = new SQLiteConnection(Path.Combine(path, "data.db3"));
            conn.CreateTable<DbProductHistory>();
        }

        public IList<Product> GetProductHistory()
        {
            return conn.Table<DbProductHistory>().OrderByDescending(c=>c.ViewTime).Select(c => c as Product).ToList();
        }

        public void AddHistory(Product item)
        {
            lock (lockData)
            {
                conn.InsertOrReplace(new DbProductHistory(item));
            }
        }

        public void DeleteHistory(Guid id)
        {
            conn.Execute("Delete FROM  ProductHistory Where ProductId=?", id);
        }


    }
}
