﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class History : ContentPage
    {
        public History()
        { 


            HistoryViewModel model = new HistoryViewModel();
            model.Init();
            this.BindingContext = model;
            InitializeComponent();
        }

        

        private async void list_ItemTapped(object sender, ItemTappedEventArgs e)
        {
         await    this.Navigation.PushAsync(new ProductDetail(e.Item as ProductViewModel));
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            var id=btn.CommandParameter;
            var vm = this.BindingContext as HistoryViewModel;
            vm.DeleteProduct((Guid)id);
        }
    }
}
