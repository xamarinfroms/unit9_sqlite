﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetailCarousel : CarouselPage
    {
        public ProductDetailCarousel()
        {
            InitializeComponent();
        }

        public ProductDetailCarousel(IList<ProductViewModel> datas)
        {
            this.ItemsSource = datas;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var page = App.Current.MainPage as MasterDetailPage;
            page.IsGestureEnabled = false;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            var page = App.Current.MainPage as MasterDetailPage;
            page.IsGestureEnabled = true;
        }
    }
}
