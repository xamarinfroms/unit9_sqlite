﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class HistoryViewModel: INotifyPropertyChanged
    {
        private ProductService service;
        public HistoryViewModel()
        {
            this.service = ((App)App.Current).ProductService;
        }

        public async void Init()
        {
            var data =await service.GetHistory();
            ObservableCollection<ProductViewModel> list = new ObservableCollection<ProductViewModel>();
            foreach (var item in data)
                list.Add(new ProductViewModel(item));
            this.HistoryList = list;
        }

        public IList<ProductViewModel> HistoryList
        {
            get { return this._historyList; }
            set { this._historyList = value; this.PropertyChanged?.DynamicInvoke(this,new PropertyChangedEventArgs(nameof(this.HistoryList))); }
        }

        public async void DeleteProduct(Guid id)
        {
            var data=this.HistoryList.SingleOrDefault(c => c.ProductId == id);
            if (data != null)
            {
                this.HistoryList.Remove(data);
                await this.service.DeleteHistory(data.ProductId);
            }
        }

        private IList<ProductViewModel> _historyList;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
